package com.shpp.app.table;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class FloatTableTest {
    private FloatTable floatTable;

    @BeforeEach
    void setUp() {
        floatTable = new FloatTable();
    }

    @Test
    void calculateFloatTablePositiveTest() {
        float[][] table = {{1.0f, 2.0f}, {2.0f, 4.0f}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(floatTable.calculate(1.0f, 2.0f, 1.0f)));
    }

    @Test
    void calculateFloatTableNegativeTest() {
        float[][] table = {{1.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, 1.0f}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(floatTable.calculate(-1.0f, 1.0f, 1.0f)));
    }

    @Test
    void calculateFloatTableTooBigIncrementTest() {
        float[][] table = {{1.0f}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(floatTable.calculate(1.0f, 5.0f, 10.0f)));
    }

    @Test
    void calculateFloatTableFailTest() {
        float[][] table = {{1.0f, 2.0f}, {3.0f, 4.0f}};
        Assertions.assertNotEquals(Arrays.deepToString(table),
                Arrays.deepToString(floatTable.calculate(1.0f, 2.0f, 1.0f)));
    }

    @Test
    void calculateFloatTableMinEqualsMaxTest() {
        float[][] table = {{1.0f}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(floatTable.calculate(1.0f, 1.0f, 1.0f)));
    }

    @Test
    void calculateTableWithZeroIncrementTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Arrays.deepToString(floatTable.calculate(1.0f, 2.0f, 0.0f)));
    }

    @Test
    void calculateTableWithMixedUpParametersTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Arrays.deepToString(floatTable.calculate(10.0f, 2.0f, 1.0f)));
    }
}