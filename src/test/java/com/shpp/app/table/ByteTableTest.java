package com.shpp.app.table;

import com.shpp.app.visitor.PrintTableVisitor;
import com.shpp.app.visitor.Visitor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class ByteTableTest {

    private ByteTable byteTable;

    @BeforeEach
    void setUp() {
        byteTable = new ByteTable();
        Visitor visitor = new PrintTableVisitor();
    }

    @Test
    void calculatePositiveTest() {
        byte[][] table = {{1, 2}, {2, 4}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(byteTable.calculate(Byte.parseByte("1"),
                        Byte.parseByte("2"), Byte.parseByte("1"))));
    }

    @Test
    void calculateNegativeTest() {
        byte[][] table = {{1, 0, -1}, {0, 0, 0}, {-1, 0, 1}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(byteTable.calculate(Byte.parseByte("-1"),
                        Byte.parseByte("1"), Byte.parseByte("1"))));
    }

    @Test
    void calculateTooBigIncrementTest() {
        byte[][] table = {{1}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(byteTable.calculate(Byte.parseByte("1"),
                        Byte.parseByte("5"), Byte.parseByte("10"))));
    }

    @Test
    void calculateFailTest() {
        byte[][] table = {{1, 2}, {3, 4}};
        Assertions.assertNotEquals(Arrays.deepToString(table),
                Arrays.deepToString(byteTable.calculate(Byte.parseByte("1"),
                        Byte.parseByte("2"), Byte.parseByte("1"))));
    }

    @Test
    void calculateMinEqualsMaxTest() {
        byte[][] table = {{1}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(byteTable.calculate(Byte.parseByte("1"),
                        Byte.parseByte("1"), Byte.parseByte("1"))));
    }

    @Test
    void calculateTableWithZeroIncrementTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Arrays.deepToString(byteTable.calculate((byte) 1, (byte)2, (byte)0)));
    }

    @Test
    void calculateTableWithMixedUpParametersTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Arrays.deepToString(byteTable.calculate((byte) 10, (byte)2, (byte)1)));
    }
}