package com.shpp.app.table;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class LongTableTest {

    private LongTable longTable;

    @BeforeEach
    void setUp() {
        longTable = new LongTable();
    }

    @Test
    void calculateLongTablePositiveTest() {
        long[][] table = {{1, 2}, {2, 4}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(longTable.calculate(Long.parseLong("1"),
                        Long.parseLong("2"), Long.parseLong("1"))));
    }

    @Test
    void calculateLongTableNegativeTest() {
        long[][] table = {{1, 0, -1}, {0, 0, 0}, {-1, 0, 1}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(longTable.calculate(Long.parseLong("-1"),
                        Long.parseLong("1"), Long.parseLong("1"))));
    }

    @Test
    void calculateLongTableTooBigIncrementTest() {
        long[][] table = {{1}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(longTable.calculate(Long.parseLong("1"),
                        Long.parseLong("5"), Long.parseLong("10"))));
    }

    @Test
    void calculateLongTableFailTest() {
        long[][] table = {{1, 2}, {3, 4}};
        Assertions.assertNotEquals(Arrays.deepToString(table),
                Arrays.deepToString(longTable.calculate(Long.parseLong("1"),
                        Long.parseLong("2"), Long.parseLong("1"))));
    }

    @Test
    void calculateLongTableMinEqualsMaxTest() {
        long[][] table = {{1}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(longTable.calculate(Long.parseLong("1"),
                        Long.parseLong("1"), Long.parseLong("1"))));
    }

    @Test
    void calculateTableWithZeroIncrementTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Arrays.deepToString(longTable.calculate(1, 2, (long) 0)));
    }

    @Test
    void calculateTableWithMixedUpParametersTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Arrays.deepToString(longTable.calculate(10, 2, (long)1)));
    }
}