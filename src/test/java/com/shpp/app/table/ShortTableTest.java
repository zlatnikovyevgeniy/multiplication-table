package com.shpp.app.table;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class ShortTableTest {

    private ShortTable shortTable;

    @BeforeEach
    void setUp() {
        shortTable = new ShortTable();
    }

    @Test
    void calculateShortTablePositiveTest() {
        short[][] table = {{1, 2}, {2, 4}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(shortTable.calculate(Short.parseShort("1"),
                        Short.parseShort("2"), Short.parseShort("1"))));
    }

    @Test
    void calculateShortTableNegativeTest() {
        short[][] table = {{1, 0, -1}, {0, 0, 0}, {-1, 0, 1}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(shortTable.calculate(Short.parseShort("-1"),
                        Short.parseShort("1"), Short.parseShort("1"))));
    }

    @Test
    void calculateShortTableTooBigIncrementTest() {
        short[][] table = {{1}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(shortTable.calculate(Short.parseShort("1"),
                        Short.parseShort("5"), Short.parseShort("10"))));
    }

    @Test
    void calculateShortTableFailTest() {
        short[][] table = {{1, 2}, {3, 4}};
        Assertions.assertNotEquals(Arrays.deepToString(table),
                Arrays.deepToString(shortTable.calculate(Short.parseShort("1"),
                        Short.parseShort("2"), Short.parseShort("1"))));
    }

    @Test
    void calculateShortTableMinEqualsMaxTest() {
        short[][] table = {{1}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(shortTable.calculate(Short.parseShort("1"),
                        Short.parseShort("1"), Short.parseShort("1"))));
    }

    @Test
    void calculateTableWithZeroIncrementTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Arrays.deepToString(shortTable.calculate((short) 1, (short) 2, (short) 0)));
    }

    @Test
    void calculateTableWithMixedUpParametersTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Arrays.deepToString(shortTable.calculate((short) 10, (short) 2, (short) 1)));
    }
}