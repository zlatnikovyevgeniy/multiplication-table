package com.shpp.app.table;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

class IntegerTableTest {

    private IntegerTable integerTable;

    @BeforeEach
    void setUp() {
        integerTable = new IntegerTable();
    }

    @Test
    void calculateIntTablePositiveTest() {
        int[][] table = {{1, 2}, {2, 4}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(integerTable.calculate(Integer.parseInt("1"),
                        Integer.parseInt("2"), Integer.parseInt("1"))));
    }

    @Test
    void calculateIntTableNegativeTest() {
        int[][] table = {{1, 0, -1}, {0, 0, 0}, {-1, 0, 1}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(integerTable.calculate(Integer.parseInt("-1"),
                        Integer.parseInt("1"), Integer.parseInt("1"))));
    }

    @Test
    void calculateIntTableTooBigIncrementTest() {
        int[][] table = {{1}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(integerTable.calculate(Integer.parseInt("1"),
                        Integer.parseInt("5"), Integer.parseInt("10"))));
    }

    @Test
    void calculateIntTableFailTest() {
        int[][] table = {{1, 2}, {3, 4}};
        Assertions.assertNotEquals(Arrays.deepToString(table),
                Arrays.deepToString(integerTable.calculate(Integer.parseInt("1"),
                        Integer.parseInt("2"), Integer.parseInt("1"))));
    }

    @Test
    void calculateIntTableMinEqualsMaxTest() {
        int[][] table = {{1}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(integerTable.calculate(Integer.parseInt("1"),
                        Integer.parseInt("1"), Integer.parseInt("1"))));
    }

    @Test
    void calculateTableWithZeroIncrementTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Arrays.deepToString(integerTable.calculate(1, 2, 0)));
    }

    @Test
    void calculateTableWithMixedUpParametersTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Arrays.deepToString(integerTable.calculate(10, 2, 1)));
    }
}