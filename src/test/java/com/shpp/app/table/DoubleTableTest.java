package com.shpp.app.table;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class DoubleTableTest {

    private DoubleTable doubleTable;

    @BeforeEach
    void setUp() {
        doubleTable = new DoubleTable();
    }


    @Test
    void calculateDoubleTablePositiveTest() {
        double[][] table = {{1.0, 2.0}, {2.0, 4.0}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(doubleTable.calculate(1.0, 2.0, 1.0)));
    }
    @Test
    void calculateNegativeTest() {
        double[][] table = {{1.0, -0.0, -1.0}, {-0.0, 0.0, 0.0}, {-1.0, 0.0, 1.0}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(doubleTable.calculate(-1.0, 1.0, 1.0)));
    }

    @Test
    void calculateTooBigIncrementTest() {
        double[][] table = {{1.0}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(doubleTable.calculate(1.0, 5.0, 10.0)));
    }

    @Test
    void calculateFailTest() {
        double[][] table = {{1.0, 2.0}, {3.0, 4.0}};
        Assertions.assertNotEquals(Arrays.deepToString(table),
                Arrays.deepToString(doubleTable.calculate(1.0, 2.0, 1.0)));
    }

    @Test
    void calculateMinEqualsMaxTest() {
        double[][] table = {{1.0}};
        Assertions.assertEquals(Arrays.deepToString(table),
                Arrays.deepToString(doubleTable.calculate(1.0, 1.0, 1.0)));
    }

    @Test
    void calculateTableWithZeroIncrementTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Arrays.deepToString(doubleTable.calculate(1.0, 2.0, 0.0)));
    }

    @Test
    void calculateTableWithMixedUpParametersTest() {
        Assertions.assertThrows(IllegalArgumentException.class, () ->
                Arrays.deepToString(doubleTable.calculate(10.0, 2.0, 1.0)));
    }
}