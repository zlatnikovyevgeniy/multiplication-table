package com.shpp.app;

import com.shpp.app.table.MultiplicationTable;
import com.shpp.app.table.Table;
import com.shpp.app.visitor.PrintTableVisitor;
import com.shpp.app.visitor.Visitor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class MultiplicationTablePrintTest {

    private MultiplicationTable mt;
    private Visitor visitor;

    @BeforeEach
    void setUp() {
        mt = new MultiplicationTable();
        visitor = new PrintTableVisitor();
    }

    @Test
    void printByteTableTest() {
        Table table = mt.create("byte");
        table.calculate((byte) 1, (byte) 2, (byte) 1);
        String[] shortTable = {"|   1|   2|", "|   2|   4|"};
        Assertions.assertArrayEquals(shortTable, table.accept(visitor));
    }

    @Test
    void printShortTableTest() {
        Table table = mt.create("short");
        table.calculate((short) 1, (short) 2, (short) 1);
        String[] byteTable = {"|     1|     2|", "|     2|     4|"};
        Assertions.assertArrayEquals(byteTable, table.accept(visitor));
    }

    @Test
    void printIntTableTest() {
        Table table = mt.create("int");
        table.calculate(1, 2, 1);
        String[] integerTable = {"|          1|          2|", "|          2|          4|"};
        Assertions.assertArrayEquals(integerTable, table.accept(visitor));
    }

    @Test
    void printLongTableTest() {
        Table table = mt.create("long");
        table.calculate(1, 2, (long) 1);
        String[] longTable = {"|                   1|                   2|",
                "|                   2|                   4|"};
        Assertions.assertArrayEquals(longTable, table.accept(visitor));
    }

    @Test
    void printFloatTableTest() {
        Table table = mt.create("float");
        table.calculate(1.0f, 2.0f, 1.0f);
        String[] floatTable = {"|          1.0|          2.0|", "|          2.0|          4.0|"};
        Assertions.assertArrayEquals(floatTable, table.accept(visitor));
    }

    @Test
    void printDoubleTableTest() {
        Table table = mt.create("double");
        table.calculate(1.0, 2.0, 1.0);
        String[] doubleTable = {"|                   1.0|                   2.0|",
                "|                   2.0|                   4.0|"};
        Assertions.assertArrayEquals(doubleTable, table.accept(visitor));
    }

}
