package com.shpp.app;

import com.shpp.app.table.MultiplicationTable;
import com.shpp.app.table.Table;
import com.shpp.app.visitor.PrintTableVisitor;
import com.shpp.app.visitor.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App {

    private static final Logger logger = LoggerFactory.getLogger(App.class);
    private static final String FILE_NAME = "config.properties";
    private static final String SYSTEM_PROPERTY_NAME = "type";

    public static void main(String[] args) {

        MultiplicationTable mt = new MultiplicationTable();
        logger.debug("A new object of MultiplicationTable is created.");

        Visitor visitor = new PrintTableVisitor();

        try {
            Table table = mt.create(System.getProperty(SYSTEM_PROPERTY_NAME, "int"));
            table.accept(visitor);
        } catch (NullPointerException e) {
            logger.error("An error occurred while executing the program. " +
                    "Please check {} for mistakes.", FILE_NAME, e);
            throw new RuntimeException("Something wrong with your configuration file!");
        }
    }
}