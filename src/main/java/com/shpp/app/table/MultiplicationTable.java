package com.shpp.app.table;

import com.shpp.app.PropertyReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class MultiplicationTable {

    private static final Logger logger = LoggerFactory.getLogger(MultiplicationTable.class);
    private static final String[] CONFIGURATION_FILE_FIELDS  = {"min","max","increment"};
    private static final String FILE_NAME = "config.properties";
    Properties configProps = PropertyReader.readProperty(FILE_NAME);

    public Table create(String type){
        logger.info("The multiplication table was calculated.");
        if(type.equalsIgnoreCase("byte")){
            ByteTable byteTable = new ByteTable();
            byteTable.calculate(Byte.parseByte(configProps.getProperty(CONFIGURATION_FILE_FIELDS[0])),
                        Byte.parseByte(configProps.getProperty(CONFIGURATION_FILE_FIELDS[1])),
                        Byte.parseByte(configProps.getProperty(CONFIGURATION_FILE_FIELDS[2])));
            return byteTable;
        } else if(type.equalsIgnoreCase("short")){
            ShortTable shortTable = new ShortTable();
            shortTable.calculate(Short.parseShort(
                        configProps.getProperty(CONFIGURATION_FILE_FIELDS[0])),
                        Short.parseShort(configProps.getProperty(CONFIGURATION_FILE_FIELDS[1])),
                        Short.parseShort(configProps.getProperty(CONFIGURATION_FILE_FIELDS[2])));
            return shortTable;
        } else if(type.equalsIgnoreCase("int")){
            IntegerTable integerTable = new IntegerTable();
            integerTable.calculate(Integer.parseInt(
                            configProps.getProperty(CONFIGURATION_FILE_FIELDS[0])),
                    Integer.parseInt(configProps.getProperty(CONFIGURATION_FILE_FIELDS[1])),
                    Integer.parseInt(configProps.getProperty(CONFIGURATION_FILE_FIELDS[2])));
            return integerTable;
        } else if(type.equalsIgnoreCase("long")) {
            LongTable longTable = new LongTable();
            longTable.calculate(Long.parseLong(
                            configProps.getProperty(CONFIGURATION_FILE_FIELDS[0])),
                    Long.parseLong(configProps.getProperty(CONFIGURATION_FILE_FIELDS[1])),
                    Long.parseLong(configProps.getProperty(CONFIGURATION_FILE_FIELDS[2])));
            return longTable;
        } else if(type.equalsIgnoreCase("float")) {
            FloatTable floatTable = new FloatTable();
            floatTable.calculate(Float.parseFloat(
                            configProps.getProperty(CONFIGURATION_FILE_FIELDS[0])),
                    Float.parseFloat(configProps.getProperty(CONFIGURATION_FILE_FIELDS[1])),
                    Float.parseFloat(configProps.getProperty(CONFIGURATION_FILE_FIELDS[2])));
            return floatTable;
        } else if(type.equalsIgnoreCase("double")) {
            DoubleTable doubleTable = new DoubleTable();
            doubleTable.calculate(Double.parseDouble(
                            configProps.getProperty(CONFIGURATION_FILE_FIELDS[0])),
                    Double.parseDouble(configProps.getProperty(CONFIGURATION_FILE_FIELDS[1])),
                    Double.parseDouble(configProps.getProperty(CONFIGURATION_FILE_FIELDS[2])));
            return doubleTable;
        }
        logger.warn("You have entered the wrong type, please try again! Available types: byte, short, " +
                        "int, long, float, double.");
        return null;
    }
}
