package com.shpp.app.table;

import com.shpp.app.visitor.Acceptable;
import com.shpp.app.visitor.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ByteTable extends Table implements Acceptable {
    private static final Logger logger = LoggerFactory.getLogger(ByteTable.class);

    private byte[][] table;

    @Override
    public byte[][] calculate(byte min, byte max, byte increment) {
        int size = determineSize(min, max, increment);

        byte[][] table = new byte[size][size];
        byte firstMultiplier, secondMultiplier, start;

        if (increment > 0) {
            firstMultiplier = secondMultiplier = min;
            start = min;
        } else {
            firstMultiplier = secondMultiplier = max;
            start = max;
        }

        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table[row].length; col++) {
                table[row][col] = checkBounds(firstMultiplier, secondMultiplier);

                firstMultiplier += increment;
            }
            firstMultiplier = start;
            secondMultiplier += increment;
        }

        this.table = table;
        return table;
    }

    private byte checkBounds(byte firstMultiplier, byte secondMultiplier) {
        long multiplication = firstMultiplier * secondMultiplier;
        if (multiplication < Byte.MIN_VALUE || multiplication > Byte.MAX_VALUE) {
            logger.warn("During multiplication " + firstMultiplier + " and " + secondMultiplier +
                    "value go out of specified type bounds. " +
                    "So, this values will be converted to 0. Please choose more convenient type.");
            return 0;
        }
        return (byte) multiplication;
    }


    @Override
    public String[] accept(Visitor visitor) {
        return visitor.print(table);
    }
}
