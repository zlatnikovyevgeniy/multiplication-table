package com.shpp.app.table;

import com.shpp.app.visitor.Acceptable;
import com.shpp.app.visitor.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class Table implements Acceptable {
    private static final Logger logger = LoggerFactory.getLogger(Table.class);
    protected int determineSize(double min, double max, double increment) {
        if (increment == 0) throw new IllegalArgumentException("Increment can't be 0!!!");
        if (min > max) throw new IllegalArgumentException("You mixed min and max up!!!");
        logger.debug("Parameters validation is successful.");

        int size = (int) Math.ceil((max - min) / Math.abs(increment)) + 1;
        if ((max - min) != 0 && Math.abs(increment) > (max - min)) size--;
        logger.debug("The multiplication table size was determined.");

        return size;
    }

    public byte[][] calculate(byte min, byte max, byte increment){
        return new byte[0][0];
    }

    public short[][] calculate(short min, short max, short increment){
        return new short[0][0];
    }

    public int[][] calculate(int min, int max, int increment) {
        return new int[0][0];
    }

    public long[][] calculate(long min, long max, long increment) {
        return new long[0][0];
    }

    public float[][] calculate(float min, float max, float increment) {
        return new float[0][0];
    }

    public double[][] calculate(double min, double max, double increment) {
        return new double[0][0];
    }

}
