package com.shpp.app.table;

import com.shpp.app.visitor.Acceptable;
import com.shpp.app.visitor.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FloatTable extends Table implements Acceptable {

    private static final Logger logger = LoggerFactory.getLogger(FloatTable.class);

    private float[][] table;

    @Override
    public float[][] calculate(float min, float max, float increment) {
        int size = determineSize(min, max, increment);

        float[][] table = new float[size][size];
        float firstMultiplier, secondMultiplier, start;

        if (increment > 0) {
            firstMultiplier = secondMultiplier = min;
            start = min;
        } else {
            firstMultiplier = secondMultiplier = max;
            start = max;
        }

        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table[row].length; col++) {
                table[row][col] = checkBounds(firstMultiplier, secondMultiplier);

                firstMultiplier += increment;
            }
            firstMultiplier = start;
            secondMultiplier += increment;
        }

        this.table = table;
        return table;
    }

    private float checkBounds(double firstMultiplier, double secondMultiplier) {
        double multiplication = firstMultiplier * secondMultiplier;
        if (multiplication < Float.MIN_VALUE || multiplication > Float.MAX_VALUE) {
            logger.warn("During multiplication " + firstMultiplier + " and " + secondMultiplier +
                    " value go out of specified type bounds. " +
                    "So, this values will be converted to 0. Please choose more convenient type.");
            return 0;
        }
        return (float) multiplication;
    }

    @Override
    public String[] accept(Visitor visitor) {
        return visitor.print(table);
    }
}
