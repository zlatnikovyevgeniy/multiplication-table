package com.shpp.app.table;

import com.shpp.app.visitor.Acceptable;
import com.shpp.app.visitor.Visitor;

public class LongTable extends Table implements Acceptable {

    private long[][] table;

    @Override
    public long[][] calculate(long min, long max, long increment) {
        int size = determineSize(min, max, increment);

        long[][] table = new long[size][size];
        long firstMultiplier, secondMultiplier, start;

        if (increment > 0) {
            firstMultiplier = secondMultiplier = min;
            start = min;
        } else {
            firstMultiplier = secondMultiplier = max;
            start = max;
        }

        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table[row].length; col++) {
                table[row][col] = firstMultiplier * secondMultiplier;

                firstMultiplier += increment;
            }
            firstMultiplier = start;
            secondMultiplier += increment;
        }

        this.table = table;
        return table;
    }

    @Override
    public String[] accept(Visitor visitor) {
        return visitor.print(table);
    }
}
