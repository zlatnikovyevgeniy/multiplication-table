package com.shpp.app.table;

import com.shpp.app.visitor.Acceptable;
import com.shpp.app.visitor.Visitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShortTable extends Table implements Acceptable {

    private static final Logger logger = LoggerFactory.getLogger(ShortTable.class);

    private short[][] table;

    @Override
    public short[][] calculate(short min, short max, short increment) {
        int size = determineSize(min, max, increment);

        short[][] table = new short[size][size];
        short firstMultiplier, secondMultiplier, start;

        if (increment > 0) {
            firstMultiplier = secondMultiplier = min;
            start = min;
        } else {
            firstMultiplier = secondMultiplier = max;
            start = max;
        }
        logger.debug("Auxiliary variables were initialized.");

        for (int row = 0; row < table.length; row++) {
            for (int col = 0; col < table[row].length; col++) {
                table[row][col] = checkBounds(firstMultiplier, secondMultiplier);

                firstMultiplier += increment;
            }
            logger.debug("The row was filled.");
            firstMultiplier = start;
            secondMultiplier += increment;
        }
        logger.debug("The table was filled.");

        this.table = table;
        return table;
    }

    private short checkBounds(short firstMultiplier, short secondMultiplier) {
        long multiplication = firstMultiplier * secondMultiplier;
        if (multiplication < Short.MIN_VALUE || multiplication > Short.MAX_VALUE) {
            logger.warn("During multiplication " + firstMultiplier + " and " + secondMultiplier +
                    "value go out of specified type bounds. " +
                    "So, this values will be converted to 0. Please choose more convenient type.");
            return 0;
        }
        return (short) multiplication;
    }

    @Override
    public String[] accept(Visitor visitor) {
        return visitor.print(table);
    }
}
