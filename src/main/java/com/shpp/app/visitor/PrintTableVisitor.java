package com.shpp.app.visitor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PrintTableVisitor implements Visitor {
    private static final Logger logger = LoggerFactory.getLogger(Visitor.class);
    private static final int BYTE_MAX_QUANTITY_OF_DIGITS = 4;
    private static final int SHORT_MAX_QUANTITY_OF_DIGITS = 6;
    private static final int INT_MAX_QUANTITY_OF_DIGITS = 11;
    private static final int LONG_MAX_QUANTITY_OF_DIGITS = 20;
    private static final int FLOAT_MAX_QUANTITY_OF_DIGITS = 13;
    private static final int DOUBLE_MAX_QUANTITY_OF_DIGITS = 22;
    public String[] print(byte[][] table) {
        String[] res = new String[table.length];

        for (int row = 0; row < table.length; row++) {
            StringBuilder ans = new StringBuilder();

            for (int col = 0; col < table[row].length; col++) {
                ans.append("|");
                ans.append(" ".repeat(BYTE_MAX_QUANTITY_OF_DIGITS -
                        String.valueOf(table[row][col]).length()));
                ans.append(table[row][col]);
            }

            ans.append("|");
            res[row] = ans.toString();
            logger.info(res[row]);
        }

        return res;
    }

    public String[] print(short[][] table) {
        String[] res = new String[table.length];

        for (int row = 0; row < table.length; row++) {
            StringBuilder ans = new StringBuilder();

            for (int col = 0; col < table[row].length; col++) {
                ans.append("|");
                ans.append(" ".repeat(SHORT_MAX_QUANTITY_OF_DIGITS -
                        String.valueOf(table[row][col]).length()));
                ans.append(table[row][col]);
            }

            ans.append("|");
            res[row] = ans.toString();
            logger.info(res[row]);
        }

        return res;
    }

    public String[] print(int[][] table) {
        String[] res = new String[table.length];

        for (int row = 0; row < table.length; row++) {
            StringBuilder ans = new StringBuilder();

            for (int col = 0; col < table[row].length; col++) {
                ans.append("|");
                ans.append(" ".repeat(INT_MAX_QUANTITY_OF_DIGITS -
                        String.valueOf(table[row][col]).length()));
                ans.append(table[row][col]);
            }

            ans.append("|");
            res[row] = ans.toString();
            logger.info(res[row]);
        }

        return res;
    }

    public String[] print(long[][] table) {
        String[] res = new String[table.length];

        for (int row = 0; row < table.length; row++) {
            StringBuilder ans = new StringBuilder();

            for (int col = 0; col < table[row].length; col++) {
                ans.append("|");
                ans.append(" ".repeat(LONG_MAX_QUANTITY_OF_DIGITS -
                        String.valueOf(table[row][col]).length()));
                ans.append(table[row][col]);
            }

            ans.append("|");
            res[row] = ans.toString();
            logger.info(res[row]);
        }

        return res;
    }

    public String[] print(float[][] table) {
        String[] res = new String[table.length];

        for (int row = 0; row < table.length; row++) {
            StringBuilder ans = new StringBuilder();

            for (int col = 0; col < table[row].length; col++) {
                ans.append("|");
                ans.append(" ".repeat(FLOAT_MAX_QUANTITY_OF_DIGITS -
                        String.valueOf(table[row][col]).length()));
                ans.append(table[row][col]);
            }

            ans.append("|");
            res[row] = ans.toString();
            logger.info(res[row]);
        }

        return res;
    }

    public String[] print(double[][] table) {
        String[] res = new String[table.length];

        for (int row = 0; row < table.length; row++) {
            StringBuilder ans = new StringBuilder();

            for (int col = 0; col < table[row].length; col++) {
                ans.append("|");
                ans.append(" ".repeat(DOUBLE_MAX_QUANTITY_OF_DIGITS -
                        String.valueOf(table[row][col]).length()));
                ans.append(table[row][col]);
            }

            ans.append("|");
            res[row] = ans.toString();
            logger.info(res[row]);
        }

        return res;
    }
}
