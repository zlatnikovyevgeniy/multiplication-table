package com.shpp.app.visitor;

public interface Acceptable {
    String[] accept(Visitor visitor);
}
