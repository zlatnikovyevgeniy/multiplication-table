package com.shpp.app.visitor;

public interface Visitor {
    String[] print(byte[][] table);
    String[] print(short[][] table);
    String[] print(int[][] table);
    String[] print(long[][] table);
    String[] print(float[][] table);
    String[] print(double[][] table);

}
